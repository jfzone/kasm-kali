FROM kasmweb/core-kali-rolling:develop

USER root

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update 
RUN apt-get upgrade -y



#kali-tools-information-gathering: Used for Open Source Intelligence (OSINT) & information gathering
#kali-tools-vulnerability: Vulnerability assessments tools
#kali-tools-web: Designed doing web applications attacks
#kali-tools-database: Based around any database attacks
#kali-tools-passwords: Helpful for password cracking attacks – Online & offline
#kali-tools-wireless: All tools based around Wireless protocols – 802.11, Bluetooth, RFID & SDR
#kali-tools-reverse-engineering: For reverse engineering binaries
#kali-tools-exploitation: Commonly used for doing exploitation
#kali-tools-social-engineering: Aimed for doing social engineering techniques
#kali-tools-sniffing-spoofing: Any tools meant for sniffing & spoofing
#kali-tools-post-exploitation: Techniques for post exploitation stage
#kali-tools-forensics: Forensic tools – Live & Offline
#kali-tools-reporting: Reporting tools


RUN apt-get install -y \
    kali-tools-information-gathering \
    kali-tools-vulnerability \
    kali-tools-web \
    kali-tools-database \
    kali-tools-passwords \
    kali-tools-reverse-engineering \
    kali-tools-exploitation \
    kali-tools-sniffing-spoofing \
    kali-tools-post-exploitation \
    kali-tools-forensics


RUN apt-get clean
